package com.vijay.Quotes;

import com.vijay.model.ApplicantInfo;
import com.vijay.util.Constants;

public class GenderPremiumCalculator implements IPremiumCalculator{
	
	@Override
	public Integer calculatePremium(ApplicantInfo appInfo) {
		int premiumBasedOnGender = 0;
		if (Constants.GENDER.equalsIgnoreCase(appInfo.getApplicantGender())) {
			premiumBasedOnGender = (Constants.BASE_PREMIUM * 2 / 100);
		}
		System.out.println("GenderPremiumCalculator.calculatePremium()"+premiumBasedOnGender);
		return premiumBasedOnGender;
	}

}
