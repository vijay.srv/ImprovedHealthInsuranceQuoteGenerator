package com.vijay.Quotes;

import com.vijay.model.ApplicantInfo;
import com.vijay.util.QuoteType;

public class QuotesFactory {

	public Integer calculatePremium(QuoteType quote, ApplicantInfo appData) {

		switch (quote) {
		case AGE: {
			return new AgePremiumCalculator().calculatePremium(appData);
		}
		case GENDER: {
			return new GenderPremiumCalculator().calculatePremium(appData);
		}
		case PRE_EXISTING: {
			return new PreExistingConditionsPremiumCalculator().calculatePremium(appData);
		}
		case HABITS: {
			return new HabitsPremiumCalculator().calculatePremium(appData);
		}
		default: {
			return 1;
		}

		}

	}

}
