package com.vijay.Quotes;

import com.vijay.model.ApplicantInfo;
import com.vijay.util.Constants;

public class HabitsPremiumCalculator implements IPremiumCalculator {

	@Override
	public Integer calculatePremium(ApplicantInfo appInfo) {
		Integer smokingPremium = 0;
		int premiumBasedOnHabits = 0;
		int alchocolPremium = 0;
		int dailyExercisePremium = 0;
		int drugsPremium = 0;

		smokingPremium = Constants.YES.equalsIgnoreCase(appInfo.getSmoking()) ? (Constants.BASE_PREMIUM * 3 / 100)
				: (Constants.BASE_PREMIUM * -3 / 100);
		alchocolPremium = Constants.YES.equalsIgnoreCase(appInfo.getAlcohol())
				? (Constants.BASE_PREMIUM * 3 / 100)
				: (Constants.BASE_PREMIUM * -3 / 100);
		dailyExercisePremium = Constants.YES.equalsIgnoreCase(appInfo.getDailyExercise())
				? (Constants.BASE_PREMIUM * -3 / 100)
				: (Constants.BASE_PREMIUM * 3 / 100);
		drugsPremium = Constants.YES.equalsIgnoreCase(appInfo.getDrugs()) ? (Constants.BASE_PREMIUM * 3 / 100)
				: (Constants.BASE_PREMIUM * -3 / 100);

		premiumBasedOnHabits = smokingPremium + alchocolPremium + dailyExercisePremium + drugsPremium;
		System.out.println("HabitsPremiumCalculator.calculatePremium()"+premiumBasedOnHabits);
		return premiumBasedOnHabits;
	}

}
