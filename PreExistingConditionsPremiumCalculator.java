package com.vijay.Quotes;

import com.vijay.model.ApplicantInfo;
import com.vijay.util.Constants;

public class PreExistingConditionsPremiumCalculator  implements IPremiumCalculator{

	@Override
	public Integer calculatePremium(ApplicantInfo appInfo) {
		
		int premiumBasedOnPreExistingConditions = 0;
		int hyperTensionPremium = 0;
		int bloodPressurePremium = 0;
		int bloodSugarPremium = 0;
		int overWeightPremium = 0;

		if (Constants.YES.equalsIgnoreCase(appInfo.getHyperTension())) {
			hyperTensionPremium = (Constants.BASE_PREMIUM * 1 / 100);
		} else if (Constants.YES.equalsIgnoreCase(appInfo.getBloodPressure())) {
			bloodPressurePremium = (Constants.BASE_PREMIUM * 1 / 100);
		} else if (Constants.YES.equalsIgnoreCase(appInfo.getBloodSugar())) {
			bloodSugarPremium = (Constants.BASE_PREMIUM * 1 / 100);
		} else if (Constants.YES.equalsIgnoreCase(appInfo.getOverWeight())) {
			overWeightPremium = (Constants.BASE_PREMIUM * 1 / 100);
		}
		premiumBasedOnPreExistingConditions = hyperTensionPremium + bloodPressurePremium + bloodSugarPremium
				+ overWeightPremium;
		System.out.println("PreExistingConditionsPremiumCalculator.calculatePremium()"+premiumBasedOnPreExistingConditions);
		return premiumBasedOnPreExistingConditions;
 
	}	
}
