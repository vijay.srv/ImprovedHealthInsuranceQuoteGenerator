package com.vijay.test;

import static org.junit.Assert.*;
import org.junit.Test;

import com.vijay.Quotes.QuoteGeneratorMain;
import com.vijay.model.ApplicantInfo;

/**
 * 
 * Junit tests for testing the HealthInsurance quote application method
 *
 */

public class QuoteGeneratorTest {

	ApplicantInfo info = null;

	/**
	 * This Method is to test the whole program by passing the sample inputs given
	 * we are anticipating 5350 as the end result, runnign the below test should return 5350 to make this successful
	 */

	@Test
	public void testCalculatePremium() {

		info = new ApplicantInfo();

		info.setApplicantAge(34);
		info.setApplicantGender("male");
		info.setApplicantName("Norman Gomes");

		info.setHyperTension("no");
		info.setBloodPressure("no");
		info.setOverWeight("yes");
		info.setBloodSugar("no");

		info.setSmoking("no");
		info.setAlcohol("yes");
		info.setDailyExercise("yes");
		info.setDrugs("no");

		assertEquals(5350, QuoteGeneratorMain.calculatePremium(info));

	}

}