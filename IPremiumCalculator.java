package com.vijay.Quotes;

import com.vijay.model.ApplicantInfo;

public interface IPremiumCalculator {
	
	Integer calculatePremium(ApplicantInfo appInfo);

}
