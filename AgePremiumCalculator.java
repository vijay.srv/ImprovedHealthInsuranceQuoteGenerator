package com.vijay.Quotes;

import com.vijay.model.ApplicantInfo;
import com.vijay.util.Constants;

public class AgePremiumCalculator implements IPremiumCalculator {

	@Override
	public Integer calculatePremium(ApplicantInfo appInfo) {
		Integer premiumBasedOnAge = 0;
		if (appInfo.getApplicantAge() < 18) {
			premiumBasedOnAge = Constants.BASE_PREMIUM;
		} else if (appInfo.getApplicantAge() > 18 && appInfo.getApplicantAge() <= 40) {
			premiumBasedOnAge = Constants.BASE_PREMIUM + (Constants.BASE_PREMIUM * 10 / 100);
		} else if (appInfo.getApplicantAge() > 40) {
			premiumBasedOnAge = Constants.BASE_PREMIUM + (Constants.BASE_PREMIUM * 20 / 100);
		}
		System.out.println("AgePremiumCalculator.calculatePremium()"+premiumBasedOnAge);
		return premiumBasedOnAge;
	}
}
