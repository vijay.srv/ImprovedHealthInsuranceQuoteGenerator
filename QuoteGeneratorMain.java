package com.vijay.Quotes;

import java.util.Scanner;

import com.vijay.model.ApplicantInfo;
import com.vijay.util.QuoteType;

public class QuoteGeneratorMain {

	public static void main(String[] args) {
		ApplicantInfo appInfo = getUserDetails();
		int netPremium = calculatePremium(appInfo);
		System.out.println("QuoteGeneratorMain.main()"+netPremium);
	}

	public static int calculatePremium(ApplicantInfo applicantData) {
		QuotesFactory factory = new QuotesFactory();
		int totalPremium = (factory.calculatePremium(QuoteType.AGE, applicantData)
				+ factory.calculatePremium(QuoteType.GENDER, applicantData)
				+ factory.calculatePremium(QuoteType.PRE_EXISTING, applicantData)
				+ factory.calculatePremium(QuoteType.HABITS, applicantData));
		System.out.println("QuoteGeneratorMain.calculatePremium()"+totalPremium);
		return totalPremium;
	}

	public static ApplicantInfo getUserDetails() {
		ApplicantInfo applicantData = new ApplicantInfo();
		Scanner dataReader = new Scanner(System.in);

		System.out.println("Please enter Applicant name");
		String name = dataReader.next();
		applicantData.setApplicantName(name);

		System.out.println("Please enter Applicant Age");
		int age = dataReader.nextInt();
		applicantData.setApplicantAge(age);

		System.out.println("Please enter Applicant Gender");
		String gender = dataReader.next();
		applicantData.setApplicantGender(gender);

		System.out.println("Does Applicant has Hypertension: (Enter Yes or No)");
		String hypertension = dataReader.next();
		applicantData.setHyperTension(hypertension);

		System.out.println("Does Applicant has BloodPressure: (Enter Yes or No)");
		String bloodPressure = dataReader.next();
		applicantData.setBloodPressure(bloodPressure);

		System.out.println("Does Applicant has BloodSugar: (Enter Yes or No)");
		String bloodSugar = dataReader.next();
		applicantData.setBloodSugar(bloodSugar);

		System.out.println("Does Applicant is  Overweight: (Enter Yes or No)");
		String overWeight = dataReader.next();
		applicantData.setOverWeight(overWeight);

		System.out.println("Does Applicant does smoking: (Enter Yes or No)");
		String smoking = dataReader.next();
		applicantData.setSmoking(smoking);

		System.out.println("Does Applicant use Alchocol: (Enter Yes or No)");
		String alchol = dataReader.next();
		applicantData.setAlcohol(alchol);

		System.out.println("Does Applicant does  DailyExercise: (Enter Yes or No)");
		String dailyExercise = dataReader.next();
		applicantData.setDailyExercise(dailyExercise);

		System.out.println("Does Applicant use drugs: (Enter Yes or No)");
		String drugs = dataReader.next();
		applicantData.setDrugs(drugs);

		dataReader.close();

		return applicantData;

	}
}